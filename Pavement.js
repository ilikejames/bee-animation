
var Pavement = function(canvasWidth) {

	var speed;

	var x = 0,
		image = new Image();

	image.src = "http://i.imgur.com/6E81a.png";
	

	this.setSpeed = function(newSpeed) {
		speed = newSpeed;
	}

 	this.update = function update(interval) {

		x = x - (speed/1000 * interval);

		if(x < (-image.width)) {
			x = x+image.width;
		}
	}

	this.draw = function draw(ctx) {
		var xPos = x;
		var width = ctx.canvas.width;

		while(xPos<width) {
			ctx.drawImage(image, xPos, 352);
			xPos+=(image.width || 100);
		}
	}


};
