
var Bee = function(canvasWidth, y, radius, speed, circleTime, color) {

	var WIDTH = 6,
		HEIGHT = 6;

	var x = canvasWidth
	
	var totalTime = 0;
		posX = 0,
		posY = 0;
	

	this.setSpeed = function(newSpeed) {
		speed = newSpeed;
	}

 	this.update = function update(interval) {

		totalTime+=(interval);

		if(totalTime>circleTime) {
			totalTime = totalTime % circleTime;
		}

		var portion = totalTime / circleTime,
			x1 = Math.cos(portion*2*Math.PI) * radius/2,
			y1 = Math.sin(portion*2*Math.PI) * radius/2;

		x-=(speed * (interval/1000));

		posX = (x-x1) % canvasWidth;
		posY = y + y1;

		// ensure looping on screen for demo
		if(posX<0) {
			posX = canvasWidth + posX;
		}

	}

	this.draw = function draw(ctx) {
		ctx.beginPath();
		ctx.lineWidth  = 1;
		ctx.strokeStyle = color; 
		ctx.strokeRect(posX, posY, WIDTH, HEIGHT);
		ctx.closePath();
	}


};
