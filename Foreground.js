var Foreground = function(ctx, speed) {
	
	var date,
		objects = [],
		isRunning = false;
		foregroundSpeed = speed;

	this.setSpeed = function(speed) {
		foregroundSpeed = speed;
		objects.forEach(function(itm) {
			itm.setSpeed(speed);
		});
	}

	this.register = function(obj) {
		obj.setSpeed(speed);
		objects.push(obj);
	}

	this.start = function() {
		date = new Date();
		
		if(!this.isRunning) {
			this.isRunning=true;
			this.update();
		}
	}

	this.stop = function() {
		this.isRunning = false;
	}

	this.update = function() {

		if(!this.isRunning) return;

		var elapsed = new Date()-date;
		date = new Date();

		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

		for(var i =0; i<objects.length; i++) {
			objects[i].update(elapsed);
			objects[i].draw(ctx);
		}

		// call async to ensure next frame update
		var self = this;
		setTimeout(this.update.bind(this), 0);
	}

}
